# Simple Tetris

Clean architecure + TDD implementation of simple tetris

### Run
	python main.py

### Run Tests
	python -m unittest discover tests

### Output
	output.txt -- the block height within the board
