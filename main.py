from model.tetris_game import TetrisGame

if __name__ == "__main__":
    result = []

    with open('input.txt', 'r') as in_file:
        for line in in_file:
            line = line.rstrip('\n')

            if len(line):
                tetris = TetrisGame()
                tokens = line.split(',')
                result.append(str(tetris.start(tokens)))
            else:
                result.append('')

    with open('output.txt', 'w') as out_file:
        for line in result:
            out_file.write(line)
            out_file.write('\n')
