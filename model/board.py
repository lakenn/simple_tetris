from typing import List
from model.piece import Piece


class Board():

    def __init__(self, width: int, height: int, grid: List[List[int]] = None):

        if grid is None:
            self.width = width
            self.height = height
            self._grid = [[0 for _ in range(width)]
                          for _ in range(height)]
        else:
            self._grid = grid
            self.width = len(self._grid[0])
            self.height = len(self._grid)

    @classmethod
    def from_grid(cls, grid: List[List[int]]) -> 'Board':
        """init grid from 2d array"""

        width = len(grid[0])
        height = len(grid)
        return cls(width, height, grid)

    @property
    def block_height(self) -> int:
        """The resulting height of the remaining blocks within the grid"""
        for pos_x, row in enumerate(self._grid):
            if 1 in row:
                return self.height - pos_x
        return 0

    def del_filled_rows(self):
        """rows drop as rows"""
        remaining_grid = [row for row in self._grid if not all(row)]
        missing_height = self.height - len(remaining_grid)
        self._grid = [[0 for _ in range(self.width)] for _ in range(missing_height)] + remaining_grid

    def add_piece(self, piece: Piece, offset_x: int, offset_y: int):
        """Add the piece to board with with the location specified"""
        for x, row in enumerate(piece):
            for y, val in enumerate(row):
                self._grid[x + offset_x][y + offset_y] += val

    def is_valid_space(self, piece: Piece, offset_x: int, offset_y: int):
        """checks for collision and returns False if parts overlap, else True"""
        for x, row in enumerate(piece.shape):
            for y, cell in enumerate(row):
                try:
                    if cell and self._grid[x + offset_x][y + offset_y]:
                        return False
                except IndexError:
                    return False
        return True

    def __iter__(self):
        return iter(self._grid)

    def __next__(self):
        return next(iter(self))

    def __str__(self):
        return '\n'.join([''.join(['*' if cell == 1 else '.' for cell in row]) for row in self._grid])

    def __repr__(self):
        return str(self._grid)
