from typing import List

from exceptions.shape_exception import ShapeNotRecognizedException


class Piece:
    # supported shapes
    piece_shapes = {
        'Q': [[1, 1],
              [1, 1]],

        'Z': [[1, 1, 0],
              [0, 1, 1]],

        'S': [[0, 1, 1],
              [1, 1, 0]],

        'T': [[1, 1, 1],
              [0, 1, 0]],

        'I': [[1, 1, 1, 1]],

        'L': [[1, 0],
              [1, 0],
              [1, 0],
              [1, 1]],

        'J': [[0, 1],
              [0, 1],
              [0, 1],
              [1, 1]],
    }

    def __init__(self, letter: str):
        self._shape = self.piece_shapes.get(letter, [[]])

        if self._shape == [[]]:
            raise ShapeNotRecognizedException

    def __iter__(self):
        return iter(self._shape)

    def __next__(self):
        return next(iter(self))

    @property
    def shape(self) -> List[List[int]]:
        return self._shape

    def __str__(self):
        return '\n'.join([''.join(['*' if cell == 1 else '.' for cell in row]) for row in self._shape])

    def __repr__(self):
        return str(self._shape)
