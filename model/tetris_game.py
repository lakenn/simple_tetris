from typing import List
from model.board import Board
from model.piece import Piece

DFFAULT_BOARD_WIDTH = 10
DEFAULT_BOARD_HEIGHT = 101


class TetrisGame:
    """Game Engine"""

    def __init__(self, board: Board = None):
        if board is None:
            self._board = Board(DFFAULT_BOARD_WIDTH, DEFAULT_BOARD_HEIGHT)
        else:
            self._board = board

    @classmethod
    def from_2d_grid(cls, grid: List[List[int]]) -> 'TetrisGame':
        """init board from 2d array"""
        board = Board.from_grid(grid)
        return cls(board)

    @property
    def board(self) -> 'Board':
        return self._board

    def _find_valid_row_idx(self, piece: Piece, col: int) -> int:
        row = 0
        next_row = 1

        while self._board.is_valid_space(piece, next_row, col):
            row = next_row
            next_row += 1

        return row

    def drop_block(self, piece: Piece, col: int):
        row = self._find_valid_row_idx(piece, col)
        self._board.add_piece(piece, row, col)
        self._board.del_filled_rows()

    def start(self, tokens: List) -> int:
        """start simulating the block falls"""

        for token in tokens:
            piece, init_col = Piece(token[0]),  int(token[1])
            self.drop_block(piece, init_col)

        return self._board.block_height
