import unittest

from model.board import Board
from model.piece import Piece


class TestBoardMethods(unittest.TestCase):

    def test_add_Q_piece_to_board(self):
        piece = Piece('Q')
        board = Board(4, 5)

        board.add_piece(piece, 3, 0)

        expected_result = '....\n' + \
                          '....\n' + \
                          '....\n' + \
                          '**..\n' + \
                          '**..'

        self.assertEqual(expected_result, str(board))

    def test_add_Z_piece_to_board(self):
        piece = Piece('Z')
        board = Board(4, 5)

        board.add_piece(piece, 3, 0)

        expected_result = '....\n' + \
                          '....\n' + \
                          '....\n' + \
                          '**..\n' + \
                          '.**.'

        self.assertEqual(expected_result, str(board))

    def test_add_S_piece_to_board(self):
        piece = Piece('S')
        board = Board(4, 5)

        board.add_piece(piece, 3, 0)

        expected_result = '....\n' + \
                          '....\n' + \
                          '....\n' + \
                          '.**.\n' + \
                          '**..'

        self.assertEqual(expected_result, str(board))

    def test_add_T_piece_to_board(self):
        piece = Piece('T')
        board = Board(4, 5)

        board.add_piece(piece, 3, 0)

        expected_result = '....\n' + \
                          '....\n' + \
                          '....\n' + \
                          '***.\n' + \
                          '.*..'

        self.assertEqual(expected_result, str(board))

    def test_add_I_piece_to_board(self):
        piece = Piece('I')
        board = Board(5, 5)

        board.add_piece(piece, 4, 0)

        expected_result = '.....\n' + \
                          '.....\n' + \
                          '.....\n' + \
                          '.....\n' + \
                          '****.'

        self.assertEqual(expected_result, str(board))

    def test_add_L_piece_to_board(self):
        piece = Piece('L')
        board = Board(5, 5)

        board.add_piece(piece, 1, 0)

        expected_result = '.....\n' + \
                          '*....\n' + \
                          '*....\n' + \
                          '*....\n' + \
                          '**...'

        self.assertEqual(expected_result, str(board))

    def test_add_J_piece_to_board(self):
        piece = Piece('J')
        board = Board(5, 5)

        board.add_piece(piece, 1, 0)

        expected_result = '.....\n' + \
                          '.*...\n' + \
                          '.*...\n' + \
                          '.*...\n' + \
                          '**...'

        self.assertEqual(expected_result, str(board))

    def test_land_on_invalid_board_location(self):
        piece = Piece('Q')
        board = Board.from_grid([[0, 0, 0, 0],
                                 [0, 0, 0, 0],
                                 [1, 1, 0, 0],
                                 [0, 0, 0, 0]])

        self.assertFalse(board.is_valid_space(piece, 1, 0))

    def test_land_on_valid_board_location(self):
        piece = Piece('Q')
        board = Board.from_grid([[0, 0, 0, 0],
                                 [0, 0, 0, 0],
                                 [1, 1, 0, 0],
                                 [0, 0, 0, 0]])

        self.assertTrue(board.is_valid_space(piece, 0, 0))

    def test_block_height(self):
        board = Board.from_grid([[0, 0, 0, 0],
                                 [0, 0, 1, 0],
                                 [1, 1, 0, 0],
                                 [0, 0, 0, 1]])

        self.assertEqual(3, board.block_height)

    def test_zero_block_height(self):
        board = Board(4, 4)
        self.assertEqual(0, board.block_height)

    def test_del_filled_rows(self):
        board = Board.from_grid([[0, 0, 1, 0],
                                 [0, 0, 1, 0],
                                 [1, 1, 1, 1],
                                 [1, 1, 1, 1]])

        board.del_filled_rows()

        expected_result = '....\n' + \
                          '....\n' + \
                          '..*.\n' + \
                          '..*.'

        self.assertEqual(expected_result, str(board))
