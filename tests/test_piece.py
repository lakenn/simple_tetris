import unittest

from exceptions.shape_exception import ShapeNotRecognizedException
from model.piece import Piece


class TestPieceMethods(unittest.TestCase):

    def test_unknown_letter_throw_ShapeNotRecognizedException(self):
        self.assertRaises(ShapeNotRecognizedException, Piece, 'X')

    def test_all_known_letter(self):
        letters = ['Q', 'Z', 'S', 'T', 'I', 'L', 'J']
        for letter in letters:
            self.assertIsNotNone(Piece(letter))
