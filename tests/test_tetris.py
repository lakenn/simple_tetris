import unittest

from model.board import Board
from model.piece import Piece
from model.tetris_game import TetrisGame


class TestTetrisMethods(unittest.TestCase):

    def setUp(self):
        self.tetris = TetrisGame(Board(10, 10))

    def test_pieces_stack_on_each_other(self):
        expected_result = '..........\n' + \
                          '..........\n' + \
                          '......**..\n' + \
                          '.....**...\n' + \
                          '..****....\n' + \
                          '....**....\n' + \
                          '.*..***...\n' + \
                          '.*..*.***.\n' + \
                          '.*..*..***\n' + \
                          '.****...**'

        pieces = [Piece('L'), Piece('J'), Piece('Q'), Piece('T'), Piece('Z'), Piece('I'), Piece('S')]
        init_cols = [1, 3, 8, 6, 4, 2, 5]

        for piece, col in zip(pieces, init_cols):
            self.tetris.drop_block(piece, col)

        self.assertEqual(expected_result, str(self.tetris.board))

    def test_blocks_move_down_by_row_level(self):
        expected_result = '..........\n' + \
                          '..........\n' + \
                          '..........\n' + \
                          '..........\n' + \
                          '..........\n' + \
                          '..........\n' + \
                          '..........\n' + \
                          '..**......\n' + \
                          '..**......\n' + \
                          '**..******'

        pieces = [Piece('Q'), Piece('I'), Piece('I'), Piece('I'), Piece('I'), Piece('I'), Piece('Q'), Piece('Q')]
        init_cols = [0, 2, 6, 0, 6, 6, 2, 4]

        for piece, col in zip(pieces, init_cols):
            self.tetris.drop_block(piece, col)

        self.assertEqual(expected_result, str(self.tetris.board))

    def test_start_game(self):
        expected_result = '..........\n' + \
                          '..........\n' + \
                          '..........\n' + \
                          '..........\n' + \
                          '..........\n' + \
                          '..........\n' + \
                          '..........\n' + \
                          '..........\n' + \
                          '..........\n' + \
                          '........**'
        line = "I0,I4,Q8"
        self.assertEqual(1, self.tetris.start(line.split(',')))
        self.assertEqual(expected_result, str(self.tetris.board))
